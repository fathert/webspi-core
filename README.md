# Angular WebSpi Core Library

> Angular WebSpi Core Library

This repository contains the core functions to access the [WebSpi](http://www.webspi.org) backend from Angular.

It features the `@webspi/core` library package: `@webspi/core` is packaged with [ng-packagr](https://github.com/dherges/ng-packagr) and then imported into an Angular CLI app.
To run the example, do the following steps:

```bash
$ yarn install
$ yarn build:lib
$ ng serve
```

