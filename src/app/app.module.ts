import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WebspiCoreModule } from '@webspi/core';
import { RouterModule } from '@angular/router';
import { TestWebspiComponent } from './test-webspi/test-webspi.component';
import { HttpClientModule } from '@angular/common/http';
import { AppErrorHandlerTestService } from './services/app-error-handler-test.service';
import { ERROR_HANDLER } from '../../webspi-core/src/public_api';
import { AuthInterceptor } from '../../webspi-core/src/services/auth-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    TestWebspiComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([{
      path: '', component: TestWebspiComponent
    }]),
    WebspiCoreModule.forRoot()
  ],
  providers: [
    {provide: ERROR_HANDLER, useClass: AppErrorHandlerTestService },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
