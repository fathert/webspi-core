import { Component, OnInit } from '@angular/core';
import { WebspiSpService, WebspiResultSet } from '@webspi/core';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-test-webspi',
  templateUrl: './test-webspi.component.html',
  styleUrls: ['./test-webspi.component.css']
})
export class TestWebspiComponent implements OnInit {

  resultSet: WebspiResultSet;
  canExecuteWebspi$ = this.ws.canExecute('WSP_SP_RslvProc');

  constructor(
    private ws: WebspiSpService
  ) {

  }

  ngOnInit() {
    this.callWebService();
  }

  callWebService() {
    console.log('Calling webservice...');
    this.ws.execute('WSP_SP_RslvProc', {
      'PRCNAM_': 'WSP_SP_RSLVPROC',
      'BASIC_': '0'
    })
    .pipe(
      map(result => result.resultSets[0])
    )
    .subscribe(
      result => this.resultSet = result
    );
  }
}
