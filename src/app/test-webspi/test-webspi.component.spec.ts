import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestWebspiComponent } from './test-webspi.component';

describe('TestWebspiComponent', () => {
  let component: TestWebspiComponent;
  let fixture: ComponentFixture<TestWebspiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestWebspiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWebspiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
