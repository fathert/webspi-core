import { Injectable } from '@angular/core';
import { Observable, empty, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { IErrorHandler } from '../../../webspi-core/src/public_api';

@Injectable()
export class AppErrorHandlerTestService implements IErrorHandler {

  public loginUrl = '/login';

  constructor(
    private router: Router,
  ) { }

  handleError(_procedure: string, _functionName: string, error: HttpErrorResponse): Observable<any> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log(`Uh, oh, something terrible just happened...${error.message}`);
    } else {
      if (error.status === 401) {
        this.doLoginRedirect();
        return empty();
      } else {
        console.log(`Houston, we have an HTTP problem...${error}`);
      }
    }
    return throwError(error);
  }

  private doLoginRedirect() {
    console.log('For example, do login redirect...');
    // if (!this.router.url.startsWith(this.loginUrl)) {
    //   this.router.navigate([this.loginUrl, {'redirectUrl': this.router.url}]);
    // }
  }
}
