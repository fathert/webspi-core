import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { WebspiSpService } from '@webspi/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private ws: WebspiSpService
  ) {
    this.ws.endpoint = '/webspi/sp';
    this.ws.connection = 'db2-i';
  }
}
