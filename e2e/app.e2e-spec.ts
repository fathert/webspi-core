import { webspiCorePage } from './app.po';

describe('webspi-core App', () => {
  let page: webspiCorePage;

  beforeEach(() => {
    page = new webspiCorePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
