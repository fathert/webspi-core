import { NgModule, ModuleWithProviders, InjectionToken } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebspiSpService } from './services/webspi-sp.service';
import { AuthenticationService } from './services/authentication.service';
import { TokenStoreService } from './services/token-store.service';
import { DefaultErrorHandlerService } from './services/default-error-handler.service';
import { ERROR_HANDLER } from './services/error-handler.service';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class WebspiCoreModule {

  public static forRoot(): ModuleWithProviders {

    return {
      ngModule: WebspiCoreModule,
      providers: [
        TokenStoreService,
        AuthenticationService,
        WebspiSpService,
        { provide: ERROR_HANDLER, useClass: DefaultErrorHandlerService }
      ]
    }
  };
}

