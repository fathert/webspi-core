import { Injectable, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

export interface IErrorHandler {
  handleError(procedure: string, functionName: string, error: HttpErrorResponse): Observable<any>;
}

export const ERROR_HANDLER = new InjectionToken<IErrorHandler>('ERROR_HANDLER');
