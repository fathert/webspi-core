import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { TokenStoreService } from './token-store.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private ts: TokenStoreService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.ts.retrieveToken();

    if (authToken) {
      const cloned = req.clone({
        headers: req.headers.set('Auth-Token', authToken)
      });
      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }

}
