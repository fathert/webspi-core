import { Injectable } from '@angular/core';

@Injectable()
export class TokenStoreService {
  retrieveToken() {
    return localStorage.getItem('auth-token');
  }

  storeToken(token: string) {
    localStorage.setItem('auth-token', token);
  }

  clearToken() {
    localStorage.setItem('auth-token', null);
  }
}
