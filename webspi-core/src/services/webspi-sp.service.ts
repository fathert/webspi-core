import { HttpClient, HttpHeaders, HttpEvent } from '@angular/common/http';
import { catchError, mapTo } from 'rxjs/operators';
import { TokenStoreService } from './token-store.service';
import { Injectable, Inject } from '@angular/core';
import { IErrorHandler, ERROR_HANDLER } from './error-handler.service';
import { Observable, of } from 'rxjs';

export interface WebspiConfig {
  endpoint?: string;
  connection?: string;
  loginUrl?: string;
}

export interface WebspiExecuteOptions {
  reportProgress?: boolean;
}

export interface WebspiParm {
  index: number;
  typeName: string;
  isArray: boolean;
  typeId: number;
  length: number;
  scale: number;
  value: any;
}

export interface WebspiInParm {
  [parm: string]: any;
}

export interface WebspiColumnMetaData {
  precision: number;
  typeName: string;
  scale: number;
  label: string;
  type: number;
}

export interface WebspiResultSet {
  columnList: string[];
  columnMetaData: { [column: string]: WebspiColumnMetaData };
  rows: { [column: string]: any }[];
}

export interface WebspiResult {
  name: string;
  schema: string;
  returnValue: number;
  parms?: { [parm: string]: WebspiParm };
  resultSets?: WebspiResultSet[];
}

export interface WebspiDescription {
  name: string;
  schema: string;
  returnValue: number;
  parms?: { [parm: string]: WebspiParm };
}

@Injectable()
export class WebspiSpService {
  public endpoint = '/webspi/sp';
  public connection = 'default';

  constructor(
    private http: HttpClient,
    private tokenStore: TokenStoreService,
    @Inject(ERROR_HANDLER) private errorHandler: IErrorHandler
  ) {}

  // Return a basic JSON description of the procedure passed.
  describe(procedure: string): Observable<WebspiDescription> {
    const url = `${this.endpoint}/${this.connection}/${procedure}`;
    const headers = this.createHeaders();

    return this.http
      .get<WebspiResult>(url, { headers })
      .pipe(
        catchError(error =>
          this.errorHandler.handleError(procedure, 'describe', error)
        )
      );
  }

  // Execute the procedure passed.
  execute(
    procedure: string,
    inputParms?: WebspiInParm,
    options?: WebspiExecuteOptions
  ): Observable<WebspiResult> {
    const url = `${this.endpoint}/${this.connection}/${procedure}`;
    const headers = this.createHeaders();

    return this.http
      .post<WebspiResult>(url, JSON.stringify({ inputParms }), {
        headers,
        reportProgress: options && !!options.reportProgress
      })
      .pipe(
        catchError(error =>
          this.errorHandler.handleError(procedure, 'execute', error)
        )
      );
  }

  // Execute the procedure passed.
  executeWithProgress(
    procedure: string,
    inputParms?: WebspiInParm
  ): Observable<HttpEvent<any>> {
    const url = `${this.endpoint}/${this.connection}/${procedure}`;
    const headers = this.createHeaders();

    return this.http
      .post<WebspiResult>(url, JSON.stringify({ inputParms }), {
        headers,
        reportProgress: true
      })
      .pipe(
        catchError(error =>
          this.errorHandler.handleError(procedure, 'execute', error)
        )
      );
  }

  // Determine whether the procedure passed can be executed by issuing
  // a HEAD request. The Webspi backend needs to implement authority
  // checking for this to work.
  canExecute(procedure: string): Observable<boolean> {
    const headers = this.createHeaders();
    const url = `${this.endpoint}/${this.connection}/${procedure}`;

    return this.http.head(url, { headers }).pipe(
      mapTo(true),
      catchError(error => {
        this.errorHandler.handleError(procedure, 'canExecute', error);
        return of(false);
      })
    );
  }

  private createHeaders(): HttpHeaders {
    return new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json');
  }
}
