import { Injectable } from '@angular/core';
import { WebspiSpService } from './webspi-sp.service';
import { TokenStoreService } from './token-store.service';
import { map, tap, catchError } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class AuthenticationService {

  private statusSubject = new BehaviorSubject<string>(null);
  status: Observable<string> = this.statusSubject.asObservable();

  constructor(
      private webspi: WebspiSpService,
      private tokenStore: TokenStoreService
    ) {
    this.init();
  }

  init() {
    this.webspi
      .execute('WAA_SP_QryCurUsr', {
        'TOKEN_': this.tokenStore.retrieveToken()
      })
      .subscribe(
        result => this.changeState(result.parms['USER_ID_'].value),
        () => this.changeState(null)
    );
  }

  login(userId: string, password: string, domain: string = 'A'): Observable<any> {
    this.logout();
    return this.webspi.execute('WAA_SP_CrtTkn', {
      'AUTH_TYPE_': domain,
      'USER_ID_': userId,
      'PASSWORD_': password
    })
    .pipe(
      map(result => {
        return {
          token: result.parms['TOKEN_'].value,
          userId: result.parms['USER_ID_'].value
        };
      }),
      tap(login => {
        this.tokenStore.storeToken(login.token);
        this.changeState(login.userId);
      }),
      catchError((e, c) => {
        return this.handleError(e);
      })
    )
  }

  logout() {
    this.tokenStore.clearToken();
    this.changeState(null);
  }

  changeState(user: string) {
    this.statusSubject.next(user);
  }

  handleError(err: any): Observable<any> {
    console.error('Error ' + err);
    return Observable.throw(err);
  }
}
