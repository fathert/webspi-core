import { Injectable, ErrorHandler } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { IErrorHandler } from './error-handler.service';

@Injectable()
export class DefaultErrorHandlerService implements IErrorHandler {

  constructor() { }

  handleError(procedureName: string, functionName: string, error: HttpErrorResponse): Observable<any> {

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log(`Error ${error.message}`);
    } else {
      console.log(`HTTP error ${error}`);
    }

    return Observable.throw(error);
  }
}
